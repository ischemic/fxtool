require 'rubygems'
require 'exchangerate'

class CalculatorController < ApplicationController
  def index
		@currencies = Exchangerate.currencies()
		@latest = Exchangerate.dates()
  end

	def convert
		begin
			rate = Exchangerate.at(params[:date],params[:from],params[:to])
			amount = (params[:amount].to_f * rate).round(2)
			payload = {'success' => true, 'amount' => '%.2f' % amount}
		rescue Exception => e
			payload = {'success' => false, 'msg' => e.message}
		end
			render json: payload
	end
end
